$(function() {

// MMENU
if ($(window).width() < 992) {
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});


}else {
  //Показать/скрыть каталог
$('.catalog-btn').click(function(event) {
  event.preventDefault();
  $(this).toggleClass('active');
  $('.catalog-menu').fadeToggle();
});
}



// Brand slider
$('.interested-slider').slick({
    infinite: true,
    arrows: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: false,
    arrows: false,
    responsive: [
   
        {
        breakpoint: 1199,
        settings: {
            slidesToShow: 4,
        }

        },
        {
        breakpoint: 991,
        settings: {
            slidesToShow: 3,
        }

        },
        {
        breakpoint: 768,
        settings: {
            slidesToShow: 3,
        }

        },
        
        {
        breakpoint: 370,
        settings: {
            slidesToShow: 2,
        }

        },

    ]
   
});



// Begin of slider-range
$( "#slider-range" ).slider({
  range: true,
  min: 100,
  max: 10000,
  values: [ 2000, 5600 ],
  slide: function( event, ui ) {
    $( "#amount" ).val( ui.values[ 0 ] );
    $( "#amount-1" ).val( ui.values[ 1 ] );
  }
});
$( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) );
$( "#amount-1" ).val( $( "#slider-range" ).slider( "values", 1 ) );

      // Изменение местоположения ползунка при вводиде данных в первый элемент input
      $("input#amount").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();
        if(parseInt(value1) > parseInt(value2)){
          value1 = value2;
          $("input#amount").val(value1);
        }
        $("#slider-range").slider("values",0,value1); 
      });
      
      // Изменение местоположения ползунка при вводиде данных в второй элемент input  
      $("input#amount-1").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();

        if(parseInt(value1) > parseInt(value2)){
          value2 = value1;
          $("input#amount-1").val(value2);
        }
        $("#slider-range").slider("values",1,value2);
      });
// END of slider-range


// Маска телефона
$(".phone-mask").mask("+7 (999) 99-99-999");



var cartSlider = $('.cart-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  dots: false,
  asNavFor: '.cart-slider__nav'
});


$('.cart-slider__nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.cart-slider',
  dots: false,
  arrows: true,
  focusOnSelect: true,
  responsive: [
  
  {
    breakpoint: 379,
    settings: {
      slidesToShow: 2,
    }
  },
  ]
});




// Календарь
$(function () {
    $('#datetimepicker').datetimepicker({
        locale: 'ru',
        format: 'DD/MM'

    });
});

// Время доставки
$(function () {
    $('#timepicker').datetimepicker({
        locale: 'ru',
        format: 'HH:mm'

    });
});




// FansyBox
 $('.fancybox').fancybox({
  closeExisting: true,
 });



// Прокрутка вверх при клике


$('a.scroll-top').click(function() {
  $('html, body').animate({
    scrollTop: 0
  }, 700);
  return false;
});



// Показать фильтры на мобыльном
if ($(window).width() < 768) {
  $('.sidebar-filter-btn').click(function() {
      $(this).toggleClass('active');
      $('.sidebar-item').fadeToggle();
  })
}


































// Begin of slider-range
$( "#slider-range" ).slider({
  range: true,
  min: 100,
  max: 10000,
  values: [ 2000, 5600 ],
  slide: function( event, ui ) {
    $( "#amount" ).val( ui.values[ 0 ] );
    $( "#amount-1" ).val( ui.values[ 1 ] );
  }
});
$( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) );
$( "#amount-1" ).val( $( "#slider-range" ).slider( "values", 1 ) );

      // Изменение местоположения ползунка при вводиде данных в первый элемент input
      $("input#amount").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();
        if(parseInt(value1) > parseInt(value2)){
          value1 = value2;
          $("input#amount").val(value1);
        }
        $("#slider-range").slider("values",0,value1); 
      });
      
      // Изменение местоположения ползунка при вводиде данных в второй элемент input  
      $("input#amount-1").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();

        if(parseInt(value1) > parseInt(value2)){
          value2 = value1;
          $("input#amount-1").val(value2);
        }
        $("#slider-range").slider("values",1,value2);
      });
// END of slider-range













})